.PHONY: build

SRC := $(shell find . -name "*.moon")
OUT := $(SRC:.moon=.lua)

build: $(OUT)
clean:
	rm $(OUT)

%.lua: %.moon
	@moonc $<
