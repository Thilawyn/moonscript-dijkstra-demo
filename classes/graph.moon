import
	pairs
	assert
	type
	tostring
	table
from _G

import Node from require "classes.node"


----------------------------------
-- Graph
-- A graph
----------------------------------
class Graph

	----------------------------------
	-- Constants
	----------------------------------
	INF: math.huge -- Infinity


	new: =>
		@relations = {}


	-- nodeExists
	-- Returns whether the graph contains a node object or not
	nodeExists: (node) =>
		@relations[node] != nil

	-- nodeNameExists
	-- Returns whether the graph contains a node of the given name or not
	nodeNameExists: (name) =>
		(@getNodeFromName name) != nil

	-- getNodeFromName
	-- Returns the node object of the given name
	getNodeFromName: (name) =>
		for node in pairs @relations
			return node if node.name == name


	-- add
	-- Adds a node object to the graph
	add: (node) =>
		assert (not @nodeExists node), "#{ node.name } is already present in the graph!"
		assert (not @nodeNameExists node.name), "A node of the same name (\"#{ node.name }\") is already present in the graph!"

		@relations[node] = {}


	-- node
	-- Filters a node parameter
	-- -- If a node name is given, it returns the associated object
	-- -- If a node object is given, it returns it
	node: (param) =>
		-- A node name has been given as a parameter
		if (type param) == "string"
			-- If an object of the given name exists, return it
			if obj = @getNodeFromName param
				return obj

			-- Elsewise, just create it
			return Node @, param

		-- If it's not a name, then it has to be a node object
		assert (@nodeExists param), "#{ param } does not belong to this graph!"
		param


	-- setRelation
	-- Sets a relation between two nodes
	setRelation: (n1, n2, weight, symmetric = true) =>
		n1 = @node n1
		n2 = @node n2

		assert (n1 != n2), "Reflective relations are not supported!"

		@relations[n1][n2] = weight
		@relations[n2][n1] = weight if symmetric

	-- relationsToString
	-- Returns a representation of the relations between the nodes (mostly intended for debugging)
	relationsToString: =>
		str = ""
		for n1, relations in pairs @relations
			for n2, weight in pairs relations
				-- Make the weight look nice
				weight = tostring(weight)
				for i = 1, 4 - weight\len!
					weight = " #{ weight }"

				str ..= "#{ n1 } ---[#{ weight }]---> #{ n2 }\n"

		str


	-- updateDistancesFrom
	-- Performs a Dijkstra algorithm to find the shortest distance to all nodes from the source
	updateDistancesFrom: (source) =>
		source = @node source

		-- Create a list containing all the nodes and give them an infinite distance
		nodes = {}
		for node in pairs @relations
			with node\from source
				.distance = @@INF -- Distance from the source
				.previous = nil   -- Previous node

			nodes[#nodes + 1] = node

		-- Set the source's distance to 0 and sort the list in increasing distance order, so the source ends up at the top
		(source\from source).distance = 0
		@sortNodesList nodes, source

		-- While there are still nodes to process
		while #nodes > 0
			-- Pop the node at the top of the list. It should be the one with the lowest distance
			node = nodes[1]
			table.remove nodes, 1

			-- We go through all neighbours to calculate the distance from the source to the neighbours via this node
			neighbours = node\neighbours!

			for i = 1, #neighbours
				neighbour = neighbours[i]
				distance  = (node\from source).distance + @relations[node][neighbour]

				-- If we found a new shortest path, change the distance of the neighbour
				if distance < (neighbour\from source).distance
					with neighbour\from source
						.distance = distance
						.previous = node

					@sortNodesList nodes, source -- Update list order

	-- sortNodesList
	-- Sorts a list of nodes in increasing distance order
	sortNodesList: (nodes, source) =>
		table.sort nodes, (n1, n2) -> (n1\from source).distance < (n2\from source).distance


:Graph
