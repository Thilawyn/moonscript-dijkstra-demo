import pairs from _G


----------------------------------
-- Node
-- A node of a graph
----------------------------------
class Node
	new: (graph, name) =>
		@graph = graph -- Graph to which the node belongs
		@name  = name  -- Name
		@paths = {}    -- Distance and path from the other nodes

		-- Add this object to the graph
		@graph\add @


	-- Metamethods
	with @__base
		.__tostring = => "(#{ @name })" -- Conversion to string


	-- neighbours
	-- Returns the neighbours of this node
	neighbours: =>
		neighbours = {}
		for node in pairs @graph.relations[@]
			neighbours[#neighbours + 1] = node

		neighbours

	-- from
	-- Returns the table containing the distance and the path from a node
	from: (source) =>
		source = @graph\node source
		@paths = {} if not @paths

		@paths


:Node
