import Graph from require "classes.graph"


--------------------------------------
-- Path
-- The shortest path between two nodes
--------------------------------------
class Path
	new: (graph, origin, destination) =>
		@graph       = graph
		@origin      = @graph\node origin
		@destination = @graph\node destination

		@updatePath!


	-- Metamethods
	with @__base
		-- Conversion to string
		.__tostring = =>
			return "There is no path between #{ @origin } and #{ @destination }." if not @path

			str = ""
			for i = 1, #@path
				str ..= "#{ @path[i] }" .. if i < #@path then " --> " else ""

			str


	-- updatePath
	-- Updates the path between the two nodes
	updatePath: (node) =>
		if not node
			@graph\updateDistancesFrom @origin -- Process distances and path from the origin

			if (@destination\from @origin).distance < Graph.INF -- If there is path between the nodes
				@path = {}
				@updatePath @destination -- Start tracing the path
			else
				@path = nil

		else
			if previous = (node\from @origin).previous
				@updatePath previous

			@path[#@path + 1] = node


:Path
