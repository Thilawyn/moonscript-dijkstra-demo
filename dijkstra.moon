import Graph from require "classes.graph"
import Path from require "classes.path"


-- Let's create a simple graph
graph = Graph!
with graph
	\setRelation "A", "B", 5, false
	\setRelation "B", "C", 5
	\setRelation "B", "D", 10
	\setRelation "C", "E", 10
	\setRelation "D", "E", 20
	\setRelation "F", "E", 1, false

print graph\relationsToString!


-- Show the path from a node to the source
origin = graph\node "A"
destination = graph\node "C"

path = Path graph, origin, destination
print "Shortest path from #{ origin } to #{ destination }:\n#{ path }"
